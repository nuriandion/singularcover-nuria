import { Component, ViewChild, Inject } from '@angular/core';
import { InsuranceService } from 'src/services/insurance.service';
import { MatPaginator, MatSort, PageEvent, Sort } from '@angular/material';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

export interface InsuranceElement {
  kindImage: string;
  brand: string;
  brandImage: string;
  id: string;
  kind: string;
  name: string;
  price: number;
  favourite: boolean;
}

export interface Input {
  name: string;
  value: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'singular-cover';
  tableLenght = 5;

  displayedColumns: string[] = ['Logo', 'Product Name', 'Insurance Company', 'Policy Kind', 'Price', 'Favourites'];

  tableData: InsuranceElement[];
  sortedData: InsuranceElement[];
  actualTableData: Array<InsuranceElement> = new Array();

  formInputs: Array<Input> = new Array();
  favouritesList: Array<InsuranceElement> = new Array();
  pageEvent: PageEvent;
  dataSource: any;

  /* Favourites modal */
  displayedColumnsModal: string[] = ['Logo', 'Product Name', 'Insurance Company', 'Policy Kind', 'Favourites'];
  modalTableData: Array<InsuranceElement> = new Array();
  actualModalTableData: Array<InsuranceElement> = new Array();
  formInputsModal: Array<Input> = new Array();
  pageEventModal: PageEvent;

  closeResult: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private insuranceService: InsuranceService,
              private modalService: NgbModal) {
    this.displayedColumns.forEach((element, i) => {
      if (i > 0 && i < 5) {
        this.formInputs.push({name: element, value: ''})
      }
    });
    this.displayedColumnsModal.forEach((element, i) => {
      if (i > 0 && i < 4) {
        this.formInputsModal.push({name: element, value: ''})
      }
    });
  }

  getList() {
    this.insuranceService.getProducts().subscribe((data: any) => {
      this.mapDataToInsuranceObject(data);
      this.search();
    });
  }

  mapDataToInsuranceObject(insuranceProducts: any) {
    this.tableData = insuranceProducts.map((element) => {
      return {
        kindImage: element['Kind-image'],
        brand: element.brand,
        brandImage: element['brand-image'],
        id: element.id,
        kind: element.kind,
        name: element.name,
        price: +element.price,
        favourite: false
      };
    });
    this.getFavourites();
  }

  getFavourites() {
    const localFavourites = JSON.parse(localStorage.getItem('favourites'))
    if (localFavourites) {
      this.favouritesList = localFavourites;
      this.tableData.forEach((item) => {
        item.favourite = this.getIfSelected(item);
      });
    }
  }

  loadTable() {
    const startPosition = this.pageEvent ? this.pageEvent.pageIndex * this.pageEvent.pageSize : 0;
    this.actualTableData = this.tableData.slice(startPosition, startPosition + this.tableLenght);
  }

  search() {
    this.formInputs.forEach(element => {
      const field = this.getKeyToFilterBy(element.name)
      if (element.value !== '') {
        this.filterProductsByField(field, element.value);
      }
    });
    this.loadTable();
  }


  filterProductsByField(field: any, value: string) {
    this.tableData = this.tableData.filter(element => element[field].toLowerCase().includes(value.toLowerCase()));
  }

  getKeyToFilterBy(name: string) {
    let field: string;
    switch (name) {
      case this.displayedColumns[1]:
        field = 'name';
        break;
      case this.displayedColumns[2]:
        field = 'brand';
        break;
      case this.displayedColumns[3]:
        field = 'kind';
        break;
      case this.displayedColumns[4]:
        field = 'price';
        break;
      default:
        break;
    }
    return field;

  }

  getIfSelected(element: InsuranceElement) {
    let elementSelected = false;
    if (this.favouritesList) {
      const elementsSelected = this.favouritesList.filter((item) => {
        return item.name === element.name;
      });
      if (elementsSelected.length > 0) {
        elementSelected = true;
      }
    }

    return elementSelected;
  }

  elementSelected(element: InsuranceElement) {
    element.favourite = !element.favourite;
    if (element.favourite) {
      this.addToFavouritesList(element);
    } else {
      this.removeFromFavouritesList(element);
    }
  }

  addToFavouritesList(element: InsuranceElement) {
    this.favouritesList.push(element);
    this.setItemToLocalStorage('favourites', JSON.stringify(this.favouritesList));
  }

  removeFromFavouritesList(element: InsuranceElement) {
    this.favouritesList.splice(this.favouritesList.indexOf(element), 1);
    this.setItemToLocalStorage('favourites', JSON.stringify(this.favouritesList));
  }

  setItemToLocalStorage(name: string, value: string) {
    localStorage.setItem(name, value);
  }

  sortData(sort: Sort) {
    const data = this.tableData;
    if (!sort.active || sort.direction === '') {
      this.tableData = data;
      return;
    }

    this.tableData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'brand': return compare(a.brand, b.brand, isAsc);
        case 'kind': return compare(a.kind, b.kind, isAsc);
        case 'price': return compare(a.price, b.price, isAsc);
        default: return 0;
      }
    });
    this.loadTable();
  }


  /* MODAL METHODS */

  open(content) {
    this.showFavourites();
    this.modalService.open(content, {size: 'lg'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
      this.getList();
    });
  }

  showFavourites() {
    this.modalTableData = this.favouritesList;
    this.loadTableModal();
  }

  getListModal() {
    this.modalTableData = this.favouritesList;
    this.searchFavourites();
  }

  searchFavourites() {
    this.formInputsModal.forEach(item => {
      const field = this.getKeyToFilterBy(item.name);
      if (item.value !== '') {
        this.filterFavouritesProductsByField(field, item.value);
      }
    });

    this.loadTableModal();
  }

  filterFavouritesProductsByField(field: any, value: string) {
    this.modalTableData = this.modalTableData.filter(element => element[field].toLowerCase().includes(value.toLowerCase()));
  }

  loadTableModal() {
    const startPosition = this.pageEventModal ? this.pageEventModal.pageIndex * this.pageEventModal.pageSize : 0;
    this.actualModalTableData = this.modalTableData.slice(startPosition, startPosition + this.tableLenght);
  }

  sortDataModal(sort: Sort) {
    const data = this.modalTableData;
    if (!sort.active || sort.direction === '') {
      this.modalTableData = data;
      return;
    }

    this.modalTableData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'brand': return compare(a.brand, b.brand, isAsc);
        case 'kind': return compare(a.kind, b.kind, isAsc);
        default: return 0;
      }
    });

    this.loadTableModal();
  }


}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

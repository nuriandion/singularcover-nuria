import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class InsuranceService {

    private insurProductsURL = '../assets/InsurProducts.json';

    constructor(private http: HttpClient) { }

    getProducts(): Observable<any>{
        return this.http.get(this.insurProductsURL);
    }

}